using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GWE.Db.Mongo;

namespace mongo_crud.Controllers
{
    public abstract class BaseController<T> : ControllerBase
    {
        protected ILogger<T> _logger;

        protected IActionResult _200(object data, string message = null) 
        {
            return this.Ok(new { Status = "success", Data = data });
        }
    }
}
