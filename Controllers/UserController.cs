﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GWE.Db.Mongo;

namespace mongo_crud.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : BaseController<UsersController>
    {
        public UsersController(ILogger<UsersController> logger)
        {
            base._logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return this._200(new UserDao().Get());
        }

        [HttpPost]
        public IActionResult Post([FromBody]User user)
        {
            return this._200(new UserDao().Create(user));
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Put(string id, [FromBody]User user)
        {
            return this._200(new UserDao().Update(id, user));
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Post(string id)
        {
            return this._200(new UserDao().Delete(id));
        }
    }
}
