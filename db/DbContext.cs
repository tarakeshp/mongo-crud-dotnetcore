using MongoDB.Bson;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace GWE.Db.Mongo
{
    public class DbContext
    {
        public const string COLLECTION_USERS = "users";
        private static MongoClient _client = null;
        private static IMongoDatabase _ctx = null;
        public static void Init(IConfiguration configuration)
        {
            try
            {
                string uri = configuration["Database:MONGODB:URI"];
                string name = configuration["Database:MONGODB:NAME"];
                _client = new MongoClient(uri);
                _ctx = _client.GetDatabase(name);
                Console.WriteLine($"Database connection successful \n{uri}/{name}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database connection Failed");
                Console.WriteLine(ex);
                throw ex;
            }
        }

        public static IMongoClient GetClient
        {
            get
            {
                return _client;
            }
        }

        public static IMongoDatabase GetContext
        {
            get
            {
                return _ctx;
            }
        }

    }
}