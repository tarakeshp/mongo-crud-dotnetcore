using System;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace GWE.Db.Mongo
{
    [BsonIgnoreExtraElements]
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("first_name")]
        [BsonRequired]
        [Required]
        public string FirstName { get; set; }

        [BsonElement("last_name")]
        [BsonRequired]
        public string LastName { get; set; }

        [BsonElement("email")]
        [BsonRequired]
        [Required]
        public string Email { get; set; }
        [BsonElement("createdAt")]
        public DateTime CreatedAt { get; set; }
        [BsonElement("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        public string FullName
        {
            get
            {
                return $"{this.FirstName} {this.LastName}";
            }
        }
    }

    public class UserDao
    {
        private IMongoCollection<User> _collection;
        public UserDao()
        {
            this._collection = DbContext.GetContext.GetCollection<User>("users"); ;
        }

        public List<User> Get() => this._collection.Find(x => true).ToList();

        public User Get(string id) =>
           _collection.Find<User>(x => x.Id == id).FirstOrDefault();

        public User Create(User user)
        {
            this._collection.InsertOne(user);
            return user;
        }

        public User Update(string id, User user)
        {
            //this._collection.UpdateOne(x => x.Id == id, user);
            _collection.ReplaceOne(x => x.Id == id, user);
            return user;
        }

        public string Delete(string id)
        {
            _collection.DeleteOne(x => x.Id == id);
            return id;
        }
    }
}


