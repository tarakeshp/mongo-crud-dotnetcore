#References

[https://developer.okta.com/blog/2020/06/29/aspnet-core-mongodb](https://)

[https://dev.to/etnicholson/developing-a-crudapi-with-asp-net-core-mongodb-docker-swagger-cf4](https://dev.to/etnicholson/developing-a-crudapi-with-asp-net-core-mongodb-docker-swagger-cf4)

#Building Docker

######

```
docker build -t mongo-crud-dotnetcore .
```

```
docker run -d -p 80:3001 --name mongo-crud-dotnetcore mongo-crud-dotnetcore
```
